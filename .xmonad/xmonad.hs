{-# LANGUAGE DeriveDataTypeable #-}

-- Xmonad modules
import XMonad

import XMonad.Config.Desktop

import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.WindowProperties
import XMonad.Util.NamedScratchpad
import qualified XMonad.Util.ExtensibleState as XS

import XMonad.Actions.Minimize
import XMonad.Actions.Submap
import XMonad.Actions.WindowGo
import XMonad.Actions.PhysicalScreens

import XMonad.Hooks.DynamicLog
-- import XMonad.Hooks.EwmhDesktops -- needed when xmonad-contrib 0.17 is released
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.Minimize
import XMonad.Hooks.SetWMName
import XMonad.Hooks.UrgencyHook as UH

import qualified XMonad.StackSet as W

import qualified XMonad.Layout.BinarySpacePartition as BSP
import XMonad.Layout.BoringWindows
import XMonad.Layout.Grid
import XMonad.Layout.Maximize
import XMonad.Layout.Minimize
import qualified XMonad.Layout.MultiToggle as MT
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed as RN
import XMonad.Layout.ShowWName
import XMonad.Layout.Spacing
import XMonad.Layout.WindowArranger
import XMonad.Layout.WindowNavigation

import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Shell

-- Data
import Data.List
import Data.Maybe
import Data.Monoid
import qualified Data.Map.Strict as Map

-- System
import System.Exit (exitSuccess)

-- Control
import Control.Monad

-- Xmobar
import Utils

myTerminal :: String
myTerminal = "alacritty"

myFont :: String
myFont = "xft:JetBrains Mono Nerd Font:antialias=true:hinting=true:bold"

myModMask :: KeyMask
myModMask = mod4Mask

myBorderWidth :: Dimension
myBorderWidth = 2

mySpacingWidth :: Integer
mySpacingWidth = 2

mySpacing :: Border
mySpacing = Border mySpacingWidth mySpacingWidth mySpacingWidth mySpacingWidth 

myWorkspaces = [show w | w <- [1..9]]

clickableWs ws s = "<action=xdotool key super+" ++ ws ++ ">" ++ s ++ "</action>"

myWSName = def
    {
    swn_font = myFont ++ ":size=100",
    swn_color = color "white",
    swn_bgcolor = color "black",
    swn_fade = 3/4
    }

myColors :: Map.Map String String
myColors = Map.fromList 
    [
        ("white", "#ebebff"),
        ("dwhite", "#bdbde8"),
        ("lgray", "#8383af"),
        ("gray", "#616181"),
        ("dgray", "#50506b"),
        ("lblack", "#3a3a58"),
        ("black", "#222234"),
        ("dblack", "#181827"),
        ("aubergine", "#c59dc5"),
        ("turquoise", "#1fffd2"),
        ("dturquoise", "#28a492"),
        ("lsky", "#9fd4ff"),
        ("sky", "#6cbeff"),
        ("sap", "#ebffa0")

    ]


color :: String -> String
color name = Map.findWithDefault "#ff0000" name myColors

myPromptConfig = def
    {
    font = myFont,
    position = Top,
    bgColor = color "black",
    fgColor = color "white",
    bgHLight = color "sky",
    fgHLight = color "white",
    promptBorderWidth = 0,
    maxComplRows = Just 10,
    autoComplete = Just 500000,
    searchPredicate = fuzzyMatch,
    sorter = fuzzySort
    }

myKeys = 
    [
      -- Applications
      ("M-<Return>", spawn $ myTerminal ++ " -e zsh"),
      ("M-w", spawn "firefox"),
      ("M-M1-w", spawn "firefox --private-window"),
      ("M-q", spawn "qalculate-gtk"),
      ("M-b", spawn "blender"),
      ("M-r", spawn "rofi -show drun"),
      ("M-<Backspace>", spawn "xkill"),
      ("M-c", spawn $ myTerminal ++ " -e ikhal"),
      ("M-t", namedScratchpadAction myScratchpads "telegram"),
      -- Prompts
      ("M-p s", shellPrompt myPromptConfig),
      -- Media keys
      ("<Print>", spawn "scrot '%m-%d-%H-%M-%S.png' -e 'mv $f ~/Pictures/screenshots/.'"),
      ("<XF86MonBrightnessUp>", spawn "brightness-up.sh 5"),
      ("<XF86MonBrightnessDown>", spawn "brightness-up.sh -5"),
      ("<XF86AudioLowerVolume>", spawn "amixer -D pulse sset Master 5%-"),
      ("<XF86AudioRaiseVolume>", spawn "amixer -D pulse sset Master 5%+"),
      ("<XF86AudioMute>", spawn "amixer -D pulse sset Master toggle"),
      ("<XF86AudioMicMute>", spawn "amixer -D pulse sset Capture toggle"),
      -- Window management
      ("M-S-c", kill),
      ("M-<Space>", sendMessage NextLayout),
      ("M-n", refresh),
      ("M-j", sendMessage $ Go D),
      ("M-k", sendMessage $ Go U),
      ("M-h", sendMessage $ Go L),
      ("M-l", sendMessage $ Go R),
      ("M-S-j", sendMessage $ Swap D),
      ("M-S-k", sendMessage $ Swap U),
      ("M-S-h", sendMessage $ Swap L),
      ("M-S-l", sendMessage $ Swap R),
      ("M-m", withFocused minimizeWindow),
      ("M-C-m", withLastMinimized maximizeWindowAndFocus),
      ("M-C-h", sendMessage Shrink),
      ("M-C-l", sendMessage Expand),
      ("M-S-t", withFocused $ windows . W.sink),
      ("M-S-m", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts),
      ("M-,", sendMessage (IncMasterN 1)),
      ("M-.", sendMessage (IncMasterN (-1))),
      ("M-S-b", sendMessage ToggleStruts),
      ("M-s", submap' SCREEN $ mkKeymap'
          [ ("h", onNextNeighbour horizontalScreenOrderer W.view)
          , ("j", onNextNeighbour verticalScreenOrderer W.view)
          , ("k", onPrevNeighbour verticalScreenOrderer W.view)
          , ("l", onPrevNeighbour horizontalScreenOrderer W.view)
          , ("S-h", onNextNeighbour horizontalScreenOrderer W.shift)
          , ("S-j", onNextNeighbour verticalScreenOrderer W.shift)
          , ("S-k", onPrevNeighbour verticalScreenOrderer W.shift)
          , ("S-l", onPrevNeighbour horizontalScreenOrderer W.shift)
          ]),
      -- Power
      ("M-S-p", submap' POWER $ mkKeymap'
          [ ("l", spawn "light-locker-command --lock")
          , ("s", spawn "systemctl suspend")
          , ("h", spawn "systemctl hibernate")
          , ("p", spawn "poweroff")
          , ("q", io exitSuccess)
          , ("r", spawn "xmonad --recompile; xmonad --restart")
          ])
    ]
    ++ [("M-" ++ show i, windows $ W.greedyView ws) | (i, ws) <- zip [1..] myWorkspaces]
    ++ [("M-S-" ++ show i, windows $ W.shift ws) | (i, ws) <- zip [1..] myWorkspaces]


-- myKeymap = \config -> mkKeymap config myKeys -- using additionlKeys instead

myScratchpads =
    [NS "telegram" "telegram-desktop" (className =? "TelegramDesktop") nonFloating]

myLayoutHook = boringWindows . avoidStruts . maximizeWithPadding 
    (fromIntegral mySpacingWidth) . spacingRaw True mySpacing True mySpacing True . 
    showWName' myWSName . MT.mkToggle (MT.single NBFULL) $
            (name "Tall"
              $ minimize 
              $ windowNavigation 
              $ smartBorders 
              $ Tall 1 (3/100) 0.5)
            ||| (name "Grid"
              $ minimize 
              $ windowNavigation 
              $ smartBorders 
              $ GridRatio $ 12/9)
            ||| (name "BSP"
              $ minimize 
              $ windowNavigation 
              $ smartBorders 
              $ BSP.emptyBSP)
            ||| (name "Full"
              $ noBorders
              $ Full)
            where name s = renamed [RN.Replace s]

myXmobarLogHook handle = dynamicLogWithPP . namedScratchpadFilterOutWorkspacePP $ def 
      { ppOutput = hPutStrLn handle
      , ppCurrent = box "Bottom" (color "sky") lineW "" . symPad padW
      , ppVisible = box "Bottom" (color "lsky") lineW "" . symPad padW
      , ppHidden = \ws -> clickableWs ws $ symPad padW ws
      , ppHiddenNoWindows = \ws -> clickableWs ws $ fc (color "dwhite") "" $ symPad padW ws
      , ppVisibleNoWindows = return $ 
              fc (color "dwhite") "" . box "Bottom" (color "sky") lineW "" . symPad padW
      , ppUrgent = box "Bottom" (color "sap") lineW "" . symPad padW
      , ppSep = " "
      , ppWsSep = padding 2
      , ppLayout = myLayoutIcon . last . words
      , ppOrder = \(ws:l:wt:xs) -> ws:l:xs
      , ppExtras = [(\kbm -> Just $ myLine kbm $ symPad padW $ show kbm) <$> XS.get]
      }
                        where padW = 4
                              lineW = 3
                              myLayoutIcon "Tall" = "<icon=tall.xpm/>"
                              myLayoutIcon "Grid" = "<icon=grid.xpm/>"
                              myLayoutIcon "BSP" = "<icon=bsp.xpm/>"
                              myLayoutIcon "Full" = "<icon=full.xpm/>"
                              myLayoutIcon x = x
                              myLine NORMAL = box "Bottom" (color "dturquoise") lineW ""
                              myLine POWER = box "Bottom" (color "sap") lineW ""
                              myLine SCREEN = box "Bottom" (color "sky") lineW ""

myStartupHook :: X ()
myStartupHook = do
    addSupported [ -- from xmonad-contrib 0.17 ewmhFullscreen
        "NET_WM_STATE",
        "_NET_WM_STATE_FULLSCREEN"
      ]
    spawnOnce "wallpaper.sh &"
    spawnOnce "picom --vsync &"
    spawnOnce "dunst &"
    spawnOnce "redshift &"
    spawnOnce "~/scripts/power/battery-notificationd &"
    spawnOnce "lxsession &"
    spawnOnce "dex --environment XMonad --autostart --search-paths \"$XDG_CONFIG_DIRS/autostart:$XDG_CONFIG_HOME/autostart\" &"

myManageHook = composeAll [ isFullscreen --> doFullFloat
                          , isDialog --> doFloat
                          ]

myConfig = withUrgencyHook NoUrgencyHook $ desktopConfig
       { 
       terminal = myTerminal,
       modMask = myModMask,
       borderWidth = myBorderWidth,
       layoutHook = myLayoutHook,
       normalBorderColor = color "black",
       focusedBorderColor = color "sky",
       workspaces = myWorkspaces,
       startupHook = myStartupHook,
       manageHook = myManageHook,
       handleEventHook = handleEventHook desktopConfig <+> fullscreenEventHook 
           <+> minimizeEventHook
      } `additionalKeysP` myKeys

main = do
     xmproc <- spawnPipe "~/.config/xmobar/my-xmobar"
     xmonad $ myConfig {logHook = logHook myConfig <+> myXmobarLogHook xmproc}

data KeybindMode = NORMAL | POWER | SCREEN deriving (Typeable,Show)

instance ExtensionClass KeybindMode where
    initialValue = NORMAL

runLogHook = ask >>= logHook . config

keybindMode :: KeybindMode -> X ()
keybindMode = XS.put

resetKeybindMode :: X ()
resetKeybindMode = keybindMode NORMAL

submap' mode keys = do
    keybindMode mode
    runLogHook
    submapDefault (resetKeybindMode >> runLogHook) (Map.map (>> resetKeybindMode) keys)

mkKeymap' = mkKeymap (def {modMask = myModMask})


-- Functions from xmonad-contrib 0.17, add ewmhFullscreen to myConfig when updated
-- and remove these functions

addSupported props = withDisplay $ \display -> do
    root <- asks theRoot
    atom <- getAtom "_NET_SUPPORTED"
    newSupported <- mapM (fmap fromIntegral . getAtom) props
    io $ do
        supportedList <- fmap (join . maybeToList) $ getWindowProperty32 display atom root
        changeProperty32 display root atom aTOM propModeReplace $ nub $ newSupported ++ supportedList

fullscreenEventHook :: Event -> X All
fullscreenEventHook (ClientMessageEvent _ _ _ dpy win typ (action:dats)) = do
  wmstate <- getAtom "_NET_WM_STATE"
  fullsc <- getAtom "_NET_WM_STATE_FULLSCREEN"
  wstate <- fromMaybe [] <$> getProp32 wmstate win

  let isFull = fromIntegral fullsc `elem` wstate

      -- Constants for the _NET_WM_STATE protocol:
      remove = 0
      add = 1
      toggle = 2
      chWstate f = io $ changeProperty32 dpy win wmstate aTOM propModeReplace (f wstate)

  when (typ == wmstate && fromIntegral fullsc `elem` dats) $ do
    when (action == add || (action == toggle && not isFull)) $ do
      chWstate (fromIntegral fullsc:)
      windows $ W.float win $ W.RationalRect 0 0 1 1
    when (action == remove || (action == toggle && isFull)) $ do
      chWstate $ delete (fromIntegral fullsc)
      windows $ W.sink win

  return $ All True

fullscreenEventHook _ = return $ All True
