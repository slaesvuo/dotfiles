#!/bin/zsh

HISTFILE=~/.cache/zsh-history
HISTSIZE=1000
SAVEHIST=5000

#shell options
setopt extendedglob
setopt autocd
unsetopt beep notify
setopt COMPLETE_ALIASES
stty stop undef

autoload -U compinit && compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' menu select
zmodload zsh/complist
_comp_options+=(globdots)

# visual stuff
autoload -U colors && colors

exitstatus() {
    [ "$?" = "0" ] || echo -n "$? "
}

promptpath() {
    pwd | sed -E 's!'"$HOME"'!~!;s!([^/]{1,3})[^/]*/!\1/!g' | tr -d '\n'
}

gitbranch() {
    branch="$(git branch --show-current 2>/dev/null | tr -d '\n' | cut -c -7)"
    [ -z "$branch" ] || echo " $branch "
}

setopt prompt_subst
PS1='%B%F{yellow}$(exitstatus)%F{blue}%n%f@%F{blue}%m%f:%b%F{12}$(promptpath) %f%F{blue}$(gitbranch)%f%(!.%F{red}#.%F{magenta}$)%f%b '

#aliases
source ~/.shell-aliases

#vi-mode
bindkey -v
export KEYTIMEOUT=1

#vim keys for tab complete
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

#different cursor shape for insert mode
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

export EDITOR=nvim
export MANPAGER="nvim -c 'set ft=man'"
export FZF_DEFAULT_COMMAND="find ."

neofetch

source ~/.config/zsh/completion-colors

#plugins
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
