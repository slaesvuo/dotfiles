import Xmobar
import Utils
import qualified Brightness as B
import qualified VolumeIcon as V
import qualified Data.Map.Strict as Map
import System.Directory

myIconDir :: FilePath -> String
myIconDir confDir = confDir ++ "/xmobar/icons"

myFont :: String
myFont = "xft:JetBrains Mono Nerd Font:size=10:bold:antialias=true"

myColors :: Map.Map String String
myColors = Map.fromList 
    [
        ("white", "#ebebff"),
        ("dwhite", "#bdbde8"),
        ("lgray", "#8383af"),
        ("gray", "#616181"),
        ("dgray", "#50506b"),
        ("lblack", "#3a3a58"),
        ("l-black", "#323248"),
        ("black", "#222234"),
        ("dblack", "#181827"),
        ("aubergine", "#c59dc5"),
        ("turquoise", "#1fffd2"),
        ("dturquoise", "#28a492"),
        ("lsky", "#9fd4ff"),
        ("sky", "#6cbeff"),
        ("sap", "#ebffa0")

    ]


color :: String -> String
color name = Map.findWithDefault "#ff0000" name myColors

myConfig :: FilePath -> Config
myConfig confDir = defaultConfig 
    { font = myFont
    , additionalFonts = []
    , position = TopSize C 100 26
    , commands = [ Run $ Memory ["-t","\xe240 <used>M/<total>M"] 10 -- 
        , Run $ MultiCpu ["-t","\xf85a <total>%"] 10 -- 
        , Run $ Volume "pulse" "Master" ["-t","<volume>%"] 5
        , Run $ DynNetwork ["-t","\xf019 <rx>kB/s \xf093 <tx>kB/s"] 10 --   
        , Run $ Battery ["-t","<acstatus>","--",
            "-o", "\xf578 <left>%", -- 
            "-O", "\xf583 <left>%", -- 
            "-i", "\xfba3 100%"] 600 -- ﮣ
        , Run $ Date "%d-%m %H:%M" "date" 600
        , Run $ Com "/home/saku/scripts/course-panel.sh" [] "course" 600
        , Run UnsafeStdinReader
        , Run $ B.def {B.icons = ["\xf5db","\xf5da","\xf5d9","\xf5dd","\xf5de","\xf5dc","\xf5df"]} 
            --        
        , Run $ V.def {V.icons = (("\xf7ca","\xfccc"),("\xfa7d","\xfa80"))}
            --  ﳌ 墳婢
        ]
    , bgColor = color "black"
    , fgColor = color "white"
    , sepChar = "%"
    , alignSep = "}{"
    , iconRoot = myIconDir confDir
    , template = " <icon=haskell.xpm/> " 
                 ++ "%UnsafeStdinReader%}%course%{"
                 ++ "%volIcon% %pulse:Master%" ++ sep
                 ++ "%brightWIcons%" ++ sep
                 ++ "%battery%" ++ sep
                 ++ "%multicpu%" ++ sep
                 ++ "%memory%" ++ sep
                 ++ "%dynnetwork%" ++ sep
                 ++ "%date% "
    }
                 where sep = fc (color "dgray") "" " | "

main = do
        xdgConf <- getXdgDirectory XdgConfig ""
        home <- getHomeDirectory
        let confDir = if null xdgConf then home ++ "/.config/" else xdgConf
        xmobar $ myConfig confDir
