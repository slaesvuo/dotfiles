#!/bin/sh

if [[ $1 == "-h" ]] || [[ $1 == "--help" ]]
then
	echo "Usage: $0 ICON-HEIGHT PANEL-BACKGROUND-COLOR"
	exit
fi

while read name height aspect bgcolor xs
do

inkscape ${name}.svg -D -o ${name}.png

# width = ceil(height * aspect-ratio)
width=$(echo "if (scale($height*$aspect)) ($height*$aspect)/1+1 else ($height*$aspect)/1" | bc)

convert -size ${width}x$height xc:$bgcolor ${name}.png[${width}x$height] -gravity center -composite ${name}.xpm

rm ${name}.png

done < icons
