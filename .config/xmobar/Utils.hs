module Utils (fc,fn,box,padding,symPad) where

ifNEmp s f = if s /= "" then f s else s

fn :: Int -> String -> String
fn f s = "<fn=" ++ show f ++ ">" ++ s ++ "</fn>"

fc :: String -> String -> String -> String
fc fg bg s = "<fc=" ++ fg ++ ifNEmp bg ("," ++) ++ ":0>" ++ s ++ "</fc>"

box :: String -> String -> Int -> String -> String -> String
box boxType color width offset s = "<box" ++ mb ++ typ ++ col ++ wid ++ off ++ ">" ++ s ++ "</box>"
    where typ = ifNEmp boxType (" type=" ++)
          col = ifNEmp color (" color=" ++)
          wid = ifNEmp (show width) (" width=" ++)
          off = ifNEmp offset (" offset=" ++)
          mb = if mod width 2 == 1 then "mb=1 mt=1" else "" -- fixes borders with odd widths

padding width = concat $ replicate width "<icon=padding/1px.xpm/>"

symPad width str = padding width ++ str ++ padding width
