# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Key, Screen, Group, Drag, Click, EzKey, KeyChord
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
import subprocess
from script_widget import ScriptWidget

from typing import List  # noqa: F401

#config variables

colors = {
        "white": "ebebff",
        "dwhite": "bdbde8",
        "lgray": "8383af",
        "gray": "616181",
        "dgray": "50506b",
        "lblack": "3a3a58",
        "black": "222234",
        "dblack": "181827",

        "aubergine": "c59dc5",
        "turquoise": "1fffd2",
        "dturquoise": "28a492",
        "lsky": "9fd4ff",
        "sky": "6cbeff",
        "sap": "ebffa0"
        }

layoutTheme = {
        "border_width": 2,
        "margin": 2,
        "border_focus": colors["sky"],
        "border_normal": colors["black"]
        }

widget_defaults = dict(
    font='JetBrains Mono Nerd Font Bold',
    fontsize=12,
    padding=3,
)

icon_font_size = 12

extension_defaults = widget_defaults.copy()

mod = "mod4"

#keybindings

keys = [
    # Switch between windows in current stack pane
    EzKey("M-j", lazy.layout.down()),
    EzKey("M-k", lazy.layout.up()),
    EzKey("M-h", lazy.layout.left()),
    EzKey("M-l", lazy.layout.right()),

    # Move windows in current stack
    EzKey("M-S-j", lazy.layout.shuffle_down()),
    EzKey("M-S-k", lazy.layout.shuffle_up()),
    EzKey("M-S-h", lazy.layout.shuffle_left()),
    EzKey("M-S-l", lazy.layout.shuffle_right()),

    # Resize windows
    EzKey("M-C-j", lazy.layout.grow_down()),
    EzKey("M-C-k", lazy.layout.grow_up()),
    EzKey("M-C-h", lazy.layout.grow_left()),
    EzKey("M-C-l", lazy.layout.grow_right()),

    EzKey("M-n", lazy.layout.normalize()),

    EzKey("M-m", lazy.spawn("qtile-cmd -o window -f toggle_maximize")),
    EzKey("M-C-S-t", lazy.spawn("qtile-cmd -o window -f toggle_minimize")),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod, "shift"], "c", lazy.window.kill()),

    EzKey("M-C-r", lazy.restart()),
    EzKey("M-C-q", lazy.shutdown()),

    # Control keybindings
    Key([], "XF86AudioMute", lazy.spawn("amixer -D pulse sset Master toggle")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -D pulse sset Master 5%+")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -D pulse sset Master 5%-")),
    Key([], "XF86AudioMicMute", lazy.spawn("amixer -D pulse sset Capture toggle")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightness-up.sh 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightness-up.sh -5")),

    # Application Keybindings
    EzKey("M-w", lazy.spawn("firefox --new-window")),
    EzKey("M-A-w", lazy.spawn("firefox --private-window")),
    EzKey("M-t", lazy.spawn("telegram-desktop")),
    EzKey("M-r", lazy.spawn("rofi -show drun")),
    EzKey("M-p", lazy.spawn("rofi-power")),
    EzKey("M-q", lazy.spawn("qalculate-gtk")),
    EzKey("M-b", lazy.spawn("blender")),
    Key([mod], "BackSpace", lazy.spawn("xkill")),
    EzKey("M-c", lazy.spawn("alacritty -e ikhal")),
    Key(["control"], "Print", lazy.spawn("scrot '%m-%d-%H-%M-%S.png' -e 'mv $f ~/Pictures/screenshots/.'")),

    # Keychords
        KeyChord([mod], "p", [
            Key([], "s", lazy.spawn("systemctl suspend")),
            Key([], "l", lazy.spawn("lock")),
            Key([], "p", lazy.spawn("poweroff")),
            Key([], "r", lazy.spawn("reboot")),
            Key([], "q", lazy.shutdown()),
            Key([], "h", lazy.spawn("systemctl hibernate"))
            ]),
        KeyChord([mod], "Return", [
            Key([], "h", lazy.spawn("alacritty -e ghci")),
            Key([], "p", lazy.spawn("alacritty -e python")),
            Key([], "Return", lazy.spawn("alacritty -e zsh")),
            Key([mod], "Return", lazy.spawn("alacritty -e zsh")),
            Key([], "v", lazy.spawn("alacritty -e nvim"))
    ])
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + control + letter of group = move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=False)),
    ])

layouts = [
    # layout.Max(),
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    layout.Columns(columns=2, **layoutTheme),
    layout.Matrix(columns=3, **layoutTheme),
    layout.MonadTall(**layoutTheme),
    # layout.MonadWide(),
    # layout.RatioTile(),
    layout.Tile(**layoutTheme),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(),
                widget.GroupBox(
                    active=colors["white"],
                    highlight_method="line",
                    disable_drag=True,
                    inactive=colors["lblack"],
                    this_current_screen_border=colors["sky"],
                    this_screen_border=colors["sky"],
                    highlight_color=colors["dblack"],
                    urgent_alert_method="line",
                    urgent_border=colors["aubergine"],
                    urgent_text=colors["aubergine"],
                    other_current_screen_border=colors["lblack"],
                    other_screen_border=colors["lblack"]
                ),
                widget.Spacer(lenght=bar.STRETCH),
                ScriptWidget(script=["/home/saku/scripts/course-panel.sh"], update_interval=60),
                widget.Spacer(lenght=bar.STRETCH),
                widget.TextBox(
                    fontsize=icon_font_size,
                    text="奔"
                    ),
                widget.PulseVolume(),
                widget.Sep(),
                widget.TextBox(
                    fontsize=icon_font_size,
                    text=""
                    ),
                widget.CPU(
                    format="{load_percent}%"
                    ),
                widget.Sep(),
                widget.TextBox(
                    fontsize=icon_font_size,
                    text=""
                    ),
                widget.Memory(format="{MemUsed}M/{MemTotal}M"),
                widget.Sep(),
                widget.TextBox(
                    fontsize=icon_font_size,
                    text=" "
                    ),
                widget.Backlight(
                    backlight_name="amdgpu_bl0",
                    format="{percent:2.0%}",
                    fmt="{}"
                    ),
                widget.Sep(),
                widget.Battery(
                    format="{char} {percent:2.0%}",
                    full_char="ﮣ",
                    charge_char="",
                    discharge_char="",
                    unknown_char="",
                    low_foreground=colors["aubergine"],
                    low_percentage=0.25,
                    notify_below=0.25,
                    show_short_text=False
                    ),
                widget.Sep(),
                widget.Systray(),
                widget.Sep(),
                widget.Clock(format='%m-%d %a %H:%M'),
            ],
            26,
            background=colors["black"],
            margin=[0,0,2,0],
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'qalculate-gtk'},
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

# autostart
@hook.subscribe.startup_once
def start_once():
    subprocess.call(['/home/saku/.config/qtile/autostart'])
