from libqtile.widget import base
from libqtile import bar
import subprocess

class ScriptWidget(base.ThreadPoolText):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
            ("script", None, "List of arguments.")
            ]

    def __init__(self, **config):
        base.ThreadPoolText.__init__(self, **config)
        self.add_defaults(ScriptWidget.defaults)
        self.text = ""

    def poll(self):
        stdout = subprocess.run(self.script, capture_output=True).stdout
        self.text = stdout.decode("utf-8").strip()
        return self.text
