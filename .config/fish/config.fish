function fish_mode_prompt
  switch $fish_bind_mode
    case default
      set_color green --bold
      echo "(N)"
    case insert
      set_color brblue --bold
      echo "(I)"
    case replace_one
      set_color brred --bold
      echo "(R)"
    case visual
      set_color brmagenta --bold
      echo "(V)"
    case "*"
      set_color red --bold 
      echo "(?)"
    end
  set_color normal
  echo " "
end

set -g __fish_git_prompt_describe_style branch
set -g __fish_git_prompt_shorten_branch_len 7
set -g fish_prompt_pwd_dir_length 3
fish_vi_key_bindings

function fish_prompt
  set_color blue --bold
  echo -n (whoami)
  set_color white
  echo -n "@"
  set_color blue
  echo -n (hostname)
  set_color white
  echo -n ":"
  set_color normal
  set_color blue
  echo -n (prompt_pwd) (fish_git_prompt | sed "s/(//" | sed "s/)//")
  set_color purple
  echo -n " ❯ "
end

set -U fish_cursor_default block
set -U fish_cursor_insert line
set -U fish_cursor_replace_one underscore
set fish_cursor_replace underscore

source ~/.config/fish/fish-aliases
set fish_greeting ""
set --export EDITOR nvim
set --export MANPAGER "nvim -c 'set ft=man'"
set --export VISUAL nvim

if status is-interactive
  neofetch
end
