set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
set background=dark

" finnish spelling
let g:vimchant_spellcheck_lang = "fi_FI.UTF-8"

filetype plugin indent on

" vim plug plugins
call plug#begin('~/.vim/plugged')

Plug 'airblade/vim-gitgutter'
Plug 'chrisbra/Colorizer'
Plug 'dag/vim-fish'
Plug 'davidhalter/jedi-vim'
Plug 'dense-analysis/ale'
Plug 'mattn/emmet-vim'
Plug 'mbbill/undotree'
" Plug 'neomake/neomake/'
Plug 'neovimhaskell/haskell-vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'SirVer/ultisnips'
Plug 'tmhedberg/SimpylFold'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'zchee/deoplete-jedi'
Plug 'enomsg/vim-haskellConcealPlus'
Plug 'itchyny/lightline.vim'

call plug#end()

" autocompletions

let g:deoplete#enable_at_startup = 1
let g:jedi#completions_enabled = 0
let g:jedi#show_call_signatures = 2

let g:markdown_fenced_languages = ['gnuplot', 'python', 'sh', 'haskell', 'html', 'groff', 'tex']
let g:pandoc#syntax#codeblocks#embeds#langs = ['gnuplot', 'python', 'sh', 'haskell', 'html', 'groff', 'tex']
let g:pandoc#syntax#codeblocks#embeds#use = 1

call deoplete#custom#var('omni', 'input_patterns', { 'pandoc': '@'})
call deoplete#custom#option('sources', {
            \ '_': ['ale'],
            \})

let g:ale_sign_column_always = 1
let g:ale_sign_error = '❌'
let g:ale_sign_warning = '!!'
let g:ale_haskell_ghc_options = '-fno-code -v0 -dynamic'
hi clear ALEErrorSign
hi clear ALEWarningSign

" pandoc config vars

let g:pandoc#formatting#mode="hA"
let g:pandoc#folding#level=0
let g:pandoc#keyboard#use_default_mappings=0

set showcmd           " Show (partial) command in status line.
set showmatch         " Show matching brackets.
set ignorecase        " Do case insensitive matching
set smartcase         " Do smart case matching
set incsearch         " Incremental search
set autowrite         " Automatically save before commands like :next and :make
set hidden            " Hide buffers when they are abandoned
set mouse=a           " Enable mouse usage (all modes)
set virtualedit=block " Enable visual block selection to go over line ends

" relative linenumbering
set number relativenumber

" split correction
set splitbelow splitright

" tab completion
set wildmode=longest,list,full

" persistent undo
set undodir=$HOME/.cache/vim-undo
set undofile

" recursive search from current dir
set path=.,,**

" theming
set noshowmode
set fillchars+=vert:╹
set termguicolors
colorscheme tone
let g:lightline = {
      \ 'colorscheme': 'tone',
      \ }
highlight GitGutterAdd guifg=#1fffd2
highlight GitGutterChange guifg=#dfa0ea
highlight GitGutterDelete guifg=#e85f68
let g:gitgutter_map_keys = 0

" use system clipboard
set clipboard+=unnamedplus

" disable search highlighting
set nohlsearch

" never conceal text on the line with the cursor
set concealcursor=""

let mapleader=" "

set tabstop=4
set shiftwidth=4
set expandtab

" keybindings
let g:jedi#rename_command = "<leader>N"
nnoremap <leader>cc :w <bar> !comp % &<CR><CR>
nnoremap <leader>co :w <bar> !comp % open &<CR><CR>
nnoremap <leader>CC :w! <bar> !comp % &<CR><CR>
nnoremap <leader>Co :w! <bar> !comp % open &<CR><CR>
nnoremap Y y$
nnoremap <leader>v :call SwapVirtualEdit()<CR>
" flipping , and ; makes more sense on a finnish keyboard layout
nnoremap , ;
nnoremap ; ,

function SwapVirtualEdit()
    if !exists('b:VEHistory')
        let b:VEHistory = 'all'
    endif
    if b:VEHistory !=# 'all' && &l:virtualedit !=# 'all'
        let &l:virtualedit = b:VEHistory
        let b:VEHistory = 'all'
    else
        let temp = b:VEHistory
        let b:VEHistory = &l:virtualedit
        let &l:virtualedit = temp
    endif
endfunction

" autocommands
autocmd BufReadPost * norm zn

let g:UltiSnipsSnippetDirectories=["UltiSnips"]
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
let g:pandoc#spell#enabled = 0
