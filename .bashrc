# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

DBLUE="\[$(tput setaf 4)\]"
LBLUE="\[$(tput setaf 12)\]"
LAUBERGINE="\[$(tput setaf 13)\]"
DSAP="\[$(tput setaf 3)\]"
BOLD="\[$(tput bold)\]"
RESET="\[$(tput sgr0)\]"

exitstatus() {
    [ "$?" = "0" ] || echo -n "$? "
}

promptpath() {
    pwd | sed -E 's!'"$HOME"'!~!;s!([^/]{1,3})[^/]*/!\1/!g' | tr -d '\n'
}

gitbranch() {
    branch="$(git branch --show-current 2>/dev/null | tr -d '\n' | cut -c -7)"
    [ -z "$branch" ] || echo " $branch "
}

PS1="${BOLD}${DSAP}\$(exitstatus)${DBLUE}\u${RESET}${BOLD}@${DBLUE}\h${RESET}${BOLD}:${RESET}${LBLUE}\$(promptpath) ${DBLUE}\$(gitbranch)${LAUBERGINE}\\\$ ${RESET}"
#PS1='\e[1m\[\e[34m\]\u\[\e[m\]\[\e[38m\]\e[1m@\[\e[m\]\[\e[34m\]\e[1m\h\[\e[m\]:\[\e[34m\]\W\[\e[m\]\[\e[35m\]\$\[\e[m\] '

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.shell-aliases ]; then
    . ~/.shell-aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export MANPAGER="nvim -c 'set ft=man'"

neofetch
shopt -s autocd
