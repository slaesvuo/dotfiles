# My dotfiles

This repository contains most of my configuration files (often called dotfiles).

## Copying

You are free to use these under the GNU GPL version 3.0 or any later version.